CREATE TABLE `auction_house_text_ui`.`buyer` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `buyer_name` VARCHAR(45) NOT NULL,
  `buyer_license_number` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE `auction_house_text_ui`.`seller` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `seller_first_name` VARCHAR(45) NOT NULL,
  `seller_last_name` VARCHAR(45) NOT NULL,
  `seller_name_lot` VARCHAR(60) NOT NULL,
  `seller_license_number` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE `auction_house_text_ui`.`lot` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name_lot` VARCHAR(45) NOT NULL,
  `seller_id` INT NOT NULL,
  PRIMARY KEY (`id`));

  REATE TABLE `auction_house_text_ui`.`bid` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `lot_id` INT NOT NULL,
  `buyer_id` INT NOT NULL,
  `seller_id` INT NOT NULL,
  `first_bet` INT NOT NULL,
  `second_bet` INT NOT NULL,
  PRIMARY KEY (`id`));