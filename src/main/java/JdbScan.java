import AddingNewData.AddDBBid;
import AddingNewData.AddDBBuyer;
import AddingNewData.AddDBLot;
import AddingNewData.AddDBSeller;

import java.sql.SQLException;
import java.util.Scanner;

/**
 * Created by Евгений on 01.09.2017.
 */

public class JdbScan {
    AddDBSeller addSeller = new AddDBSeller();
    AddDBBuyer addBuyer = new AddDBBuyer();
    AddDBBid addBid = new AddDBBid();
    AddDBLot addLot = new AddDBLot();

    Scanner scanner = new Scanner(System.in);
    public void menu(){
        System.out.println("если вы хотите добавить продавца нажмите 1");
        System.out.println("если вы хотите добавить покупателя нажмите 2");
        System.out.println("если вы хотите добавить товар нажмите 3");
        System.out.println("если вы хотите сделать ставку нажмите 4");
        System.out.println("если вы хотите выйти нажмите 5");
    }
    public void switchMenu() throws SQLException {
        int selectionOut = scanner.nextInt();
        while(selectionOut != 5) {
            menu();

            int selection = scanner.nextInt();
            switch (selection) {
                case 1:
                    addSeller.addSeller();
                    menu();
                    break;
                case 2:
                    addBuyer.addBuyer();
                    menu();
                    break;
                case 3:
                    addLot.addLot();
                    menu();
                    break;
                case 4:
                    addBid.addBid();
                    menu();
                    break;
                default:
                    System.out.println("Error");

            }
        }
    }
}
