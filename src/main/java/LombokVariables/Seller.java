package LombokVariables;

/**
 * Created by Евгений on 01.09.2017.
 */
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Seller { //продавец
    private int id;
    private String firstName;
    private String lastName;
    private String nameLot;


    @Override
    public String toString(){
        return getClass().getSimpleName() + " " +id + " firstName: " +firstName+ " lastName: " + lastName+ " nameLot: "+ nameLot;
    }
}

