package LombokVariables;

/**
 * Created by Евгений on 01.09.2017.
 */
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Lot {
    private int id;
    private String name;
    private int sellerId;


    @Override
    public String toString(){
        return getClass().getSimpleName() + "id: " +id + " Name: " +name+ " sellerId : " + sellerId;
    }
}
