package LombokVariables;

/**
 * Created by Евгений on 01.09.2017.
 */
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Bid { private int id;

    public int lotId;
    public int sellerId;
    public int buyerId;
    public int firstBet;
    public int secondBet;


    @Override
    public String toString(){
        return getClass().getSimpleName() + "id: " +id + " sellerId: " +sellerId+ " lotId: " + lotId+ " buyerId: "+ buyerId+
                " firstBet: "+firstBet+ " secondBet:"+ secondBet;

    }

}