package LombokVariables;

/**
 * Created by Евгений on 01.09.2017.
 */
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Buyer { //покупатель

    private int id;
    private String name;

    @Override
    public String toString(){
        return getClass().getSimpleName() + "id: " +id + " name: "+ name + " ";
    }
}
